#include <iostream>
#include <vector>

// https://www.hackerrank.com/challenges/operator-overloading/problem?isFullScreen=true

class Matrix {
 public:
  Matrix operator+(Matrix &other);
  std::vector<std::vector<int>> a;
};
Matrix Matrix::operator+(Matrix &other) {
  Matrix mtemp;
  for (int i = 0; i < a.size(); i++) {
    std::vector<int> temp;
    for (int j = 0; j < a[i].size(); j++) {
      temp.push_back(a[i][j] + other.a[i][j]);
    }
    mtemp.a.push_back(temp);
  }
  return mtemp;
}

int main() {
  int cases, k;
  std::cin >> cases;
  for (k = 0; k < cases; k++) {
    Matrix x;
    Matrix y;
    Matrix result;
    int n, m, i, j;
    std::cin >> n >> m;
    for (i = 0; i < n; i++) {
      std::vector<int> b;
      int num;
      for (j = 0; j < m; j++) {
        std::cin >> num;
        b.push_back(num);
      }
      x.a.push_back(b);
    }
    for (i = 0; i < n; i++) {
      std::vector<int> b;
      int num;
      for (j = 0; j < m; j++) {
        std::cin >> num;
        b.push_back(num);
      }
      y.a.push_back(b);
    }
    result = x + y;
    for (i = 0; i < n; i++) {
      for (j = 0; j < m; j++) {
        std::cout << result.a[i][j] << " ";
      }
      std::cout << std::endl;
    }
  }
  return 0;
}