#include <iostream>
#include <sstream>

// https://www.hackerrank.com/challenges/overload-operators/problem?isFullScreen=true

class Complex {
  friend std::ostream& operator<<(std::ostream& out, const Complex& obj);

 public:
  void input(const std::string& input);
  Complex operator+(const Complex& other);

 private:
  int a, b;
};
void Complex::input(const std::string& input) {
  std::stringstream ss(input);
  char temp;
  ss >> a >> temp >> temp >> b;
}
Complex Complex::operator+(const Complex& other) {
  this->a += other.a;
  this->b += other.b;
  return *this;
}
std::ostream& operator<<(std::ostream& out, const Complex& obj) {
  out << obj.a << "+i" << obj.b;
  return out;
}
int main() {
  Complex x, y;
  std::string s1, s2;
  std::cin >> s1;
  std::cin >> s2;
  x.input(s1);
  y.input(s2);
  Complex z = x + y;
  std::cout << z << std::endl;
}