#include <iostream>

// https://www.hackerrank.com/challenges/c-class-templates/problem?isFullScreen=true

#define endl '\n';
void io_sync_off() {
  // 關閉 iostream & stdio 的同步 & 使用相同的stream buffer
  std::ios_base::sync_with_stdio(false);
  // 取消 cin cout 的綁定
  std::cin.tie(nullptr);
}

template <class T>
class AddElements {
  T element;

 public:
  AddElements(T arg) { element = arg; }
  T add(T arg) { return element + arg; }
  T concatenate(T arg) { return element + arg; }
};

int main() {
  io_sync_off();
  int n, i;
  std::cin >> n;
  for (i = 0; i < n; i++) {
    std::string type;
    std::cin >> type;

    if (type == "float") {
      double element1, element2;
      std::cin >> element1 >> element2;
      AddElements<double> myfloat(element1);
      std::cout << myfloat.add(element2) << endl;
    } else if (type == "int") {
      int element1, element2;
      std::cin >> element1 >> element2;
      AddElements<int> myint(element1);
      std::cout << myint.add(element2) << endl;
    } else if (type == "string") {
      std::string element1, element2;
      std::cin >> element1 >> element2;
      AddElements<std::string> mystring(element1);
      std::cout << mystring.concatenate(element2) << endl;
    }
  }
  return 0;
}