#include <iostream>
#include <vector>

#define FUNCTION(name, operator) \
  void name(int &a, int &b) { a operator b ? false : a = b; }
#define foreach(v, i) for (int i = 0; i < v.size(); ++i)
#define io(v) std::cin >> v
#define INF 0
#define toStr(str) #str

#if !defined toStr || !defined io || !defined FUNCTION || !defined INF
#error Missing preprocessor definitions
#endif

FUNCTION(minimum, <)
FUNCTION(maximum, >)

int main() {
  int n;
  std::cin >> n;
  std::vector<int> v(n);
  foreach (v, i) { io(v)[i]; }
  int mn = INF;
  int mx = -INF;
  foreach (v, i) {
    minimum(mn, v[i]);
    maximum(mx, v[i]);
  }
  int ans = mx - mn;
  std::cout << toStr(Result =) << ' ' << ans;
  return 0;
}