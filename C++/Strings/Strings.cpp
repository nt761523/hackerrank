#include <iostream>

// https://www.hackerrank.com/challenges/c-tutorial-strings/problem?isFullScreen=true

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  std::string s1, s2, ab, a, b;
  std::cin >> s1 >> s2;
  ab = s1 + s2;
  a = s1;
  b = s2;

  a[0] = s2[0];
  b[0] = s1[0];
  std::cout << s1.size() << ' ' << s2.size() << '\n'
            << ab + '\n'
            << a << ' ' << b;
  return 0;
}