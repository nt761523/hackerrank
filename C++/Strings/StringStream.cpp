#include <iostream>
#include <sstream>
#include <vector>

// https://www.hackerrank.com/challenges/c-tutorial-stringstream/problem?isFullScreen=true

std::vector<int> parseInts(std::string& str) {
  // Complete this function
  std::stringstream ss(str);
  static std::vector<int> temp;
  char ch;
  int t;
  while (ss >> t) {
    temp.push_back(t);
    ss >> ch;
  }
  return temp;
}

int main() {
  std::string str;
  std::cin >> str;
  const std::vector<int>& integers = parseInts(str);
  for (const auto i : integers) std::cout << i << '\n';
  return 0;
}