#include <iostream>

// https://www.hackerrank.com/challenges/rectangle-area/problem?isFullScreen=true

class Rectangle {
 public:
  void display();
  //  private:
  int width, height;
};
void Rectangle::display() { std::cout << width << ' ' << height << '\n'; }

class RectangleArea : public Rectangle {
 public:
  void read_input();
  void display();
};
void RectangleArea::read_input() {
  std::cout << width << ' ' << height << '\n';
}
void RectangleArea::display() { std::cout << width * height; }

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  RectangleArea Ra;
  int width, height;
  std::cin >> width >> height;

  Ra.width = width;
  Ra.height = height;

  Ra.read_input();
  Ra.display();

  return 0;
}