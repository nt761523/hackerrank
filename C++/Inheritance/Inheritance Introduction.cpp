#include <iostream>

// https://www.hackerrank.com/challenges/inheritance-introduction/problem?isFullScreen=true

class Triangle {
 public:
  void triangle();
};
void Triangle::triangle() { std::cout << "I am a triangle\n"; }

class Isosceles : public Triangle {
 public:
  void isosceles();
  void isoscelesside();
};
void Isosceles::isosceles() { std::cout << "I am an isosceles triangle\n"; }
void Isosceles::isoscelesside() {
  std::cout << "In an isosceles triangle two sides are equal\n";
}

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  Isosceles isc;
  isc.isosceles();
  isc.isoscelesside();
  isc.triangle();
  return 0;
}