#include <iostream>

// https://www.hackerrank.com/challenges/https://www.hackerrank.com/challenges/multi-level-inheritance-cpp/problem?isFullScreen=true

class Triangle {
 public:
  void triangle();
};
void Triangle::triangle() { std::cout << "I am a triangle\n"; }

class Isosceles : public Triangle {
 public:
  void isosceles();
};
void Isosceles::isosceles() { std::cout << "I am an isosceles triangle\n"; }

class Equilateral : public Isosceles {
 public:
  void equilateral();
};
void Equilateral::equilateral() {
  std::cout << "I am an equilateral triangle\n";
}

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  Equilateral obj;

  obj.equilateral();
  obj.isosceles();
  obj.triangle();

  return 0;
}