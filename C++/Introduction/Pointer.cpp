#include <iostream>

// https://www.hackerrank.com/challenges/c-tutorial-pointer/problem?isFullScreen=true

void update(int *a, int *b) {
  int na = *a + *b;
  int nb = *a - *b;
  std::cout << na << std::endl;
  std::cout << abs(nb) << std::endl;
}

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  int a, b;
  std::cin >> a >> b;
  update(&a, &b);
  return 0;
}