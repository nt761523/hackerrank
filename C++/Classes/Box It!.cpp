#include <iostream>

// https://www.hackerrank.com/challenges/box-it/problem?isFullScreen=true

class Box {
  friend bool operator<(Box& b1, Box& b2);
  friend std::ostream& operator<<(std::ostream& out, Box& b1);

 public:
  Box();
  Box(int l, int b, int h);
  Box(const Box& B);
  int getLength();
  int getBreadth();
  int getheight();
  long long CalculateVolume();

 private:
  int l, b, h;
};
Box::Box() : l(0), b(0), h(0) {}
Box::Box(int l, int b, int h) : l(l), b(b), h(h) {}
Box::Box(const Box& B) {
  l = B.l;
  b = B.b;
  h = B.h;
}
int Box::getLength() { return l; }
int Box::getBreadth() { return b; }
int Box::getheight() { return h; }
long long Box::CalculateVolume() { return (long long)l * b * h; }
bool operator<(Box& b1, Box& b2) {
  if ((b1.l < b2.l) || (b1.l == b2.l && b1.b < b2.b) ||
      (b1.l == b2.l && b1.b == b2.b && b1.h < b2.h))
    return (true);
  else
    return (false);
}
std::ostream& operator<<(std::ostream& out, Box& b1) {
  out << b1.l << " " << b1.b << " " << b1.h;
  return out;
}

void typefunc(int type, Box& temp) {
  switch (type) {
    case 1:
      std::cout << temp << std::endl;
      break;
    case 2: {
      int l, b, h;
      std::cin >> l >> b >> h;
      Box NewBox(l, b, h);
      temp = NewBox;
      std::cout << temp << std::endl;
      break;
    }
    case 3: {
      int l, b, h;
      std::cin >> l >> b >> h;
      Box NewBox(l, b, h);
      NewBox < temp ? std::cout << "Lesser\n" : std::cout << "Greater\n";
    } break;
    case 4:
      std::cout << temp.CalculateVolume() << std::endl;
      break;
    case 5:
      Box NewBox(temp);
      std::cout << NewBox << std::endl;
      break;
  }
}

void check2() {
  int n;
  std::cin >> n;
  Box temp;
  for (int i = 0; i < n; i++) {
    int type;
    std::cin >> type;
    typefunc(type, temp);
  }
}
int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  check2();
  return 0;
}
