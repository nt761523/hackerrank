#include <iostream>

// https://www.hackerrank.com/challenges/classes-objects/problem?isFullScreen=true

class Student {
 public:
  void input(int a, int b, int c, int d, int e);
  int calculateTotalScore();

 private:
  int a, b, c, d, e;
};
void Student::input(int a, int b, int c, int d, int e) {
  this->a = a;
  this->b = b;
  this->c = c;
  this->d = d;
  this->e = e;
}
int Student::calculateTotalScore() {
  int temp = a + b + c + d + e;
  return temp;
}

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  int numberofstudents;
  int a, b, c, d, e;
  std::cin >> numberofstudents;
  Student Arrs[numberofstudents];
  for (int i = 0; i < numberofstudents; i++) {
    std::cin >> a >> b >> c >> d >> e;
    Arrs[i].input(a, b, c, d, e);
  }
  int higherscorecount = 0;
  for (int i = 1; i < numberofstudents; i++) {
    Arrs[0].calculateTotalScore() < Arrs[i].calculateTotalScore()
        ? higherscorecount++
        : numberofstudents;
  }

  std::cout << higherscorecount;
  return 0;
}