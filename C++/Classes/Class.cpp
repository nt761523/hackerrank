#include <iostream>
#include <sstream>

// https://www.hackerrank.com/challenges/c-tutorial-class/problem?isFullScreen=true

class Student {
 public:
  void set_age(const int& temp);
  int get_age();
  void set_first_name(std::string s);
  std::string& get_first_name();
  void set_last_name(std::string s);
  std::string& get_last_name();
  void set_standard(const int& temp);
  int get_standard();
  std::string to_string();

 private:
  int age, standard;
  std::string first_name, last_name;
};
void Student::set_age(const int& temp) { age = temp; }
int Student::get_age() { return age; }
void Student::set_first_name(std::string s) { first_name = s; }
std::string& Student::get_first_name() { return first_name; }
void Student::set_last_name(std::string s) { last_name = s; }
std::string& Student::get_last_name() { return last_name; }
void Student::set_standard(const int& temp) { standard = temp; }
int Student::get_standard() { return standard; }

std::string Student::to_string() {
  std::stringstream ss;
  ss << this->age << ',' << this->first_name << ',' << this->last_name << ','
     << this->standard;
  std::string s = ss.str();
  return s;
}

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  int age, standard;
  std::string first_name, last_name;
  std::cin >> age >> first_name >> last_name >> standard;

  Student s1;
  s1.set_age(age);
  s1.set_first_name(first_name);
  s1.set_last_name(last_name);
  s1.set_standard(standard);

  std::cout << s1.get_age() << '\n'
            << s1.get_last_name() << ", " << s1.get_first_name() << '\n'
            << s1.get_standard() << '\n'
            << '\n';
  std::cout << s1.to_string();

  return 0;
}