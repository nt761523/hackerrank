#include <iostream>
#include <string>

// https://www.hackerrank.com/challenges/virtual-functions/problem?isFullScreen=true

class Person {
  std::string m_name;
  int m_age;

 public:
  virtual void getdata() = 0;
  virtual void putdata() = 0;
  void getbase(std::string name, int age);
  std::string putbase();
};
void Person::getbase(std::string name, int age) {
  this->m_name = name;
  this->m_age = age;
}
std::string Person::putbase() {
  return m_name + ' ' + std::to_string(m_age) + ' ';
}

class Professor : public Person {
  int publications, cur_id;
  static int id;

 public:
  Professor();
  void getdata();
  void putdata();
};
int Professor::id = 0;
Professor::Professor() {
  this->id++;
  this->cur_id = this->id;
}
void Professor::getdata() {
  std::string temp_name;
  int tempage, temp_publications;

  std::cin >> temp_name >> tempage >> temp_publications;
  this->getbase(temp_name, tempage);
  this->publications = temp_publications;
}

void Professor::putdata() {
  std::cout << this->putbase() << this->publications << ' ' << this->cur_id
            << '\n';
}

class Student : public Person {
  int marks[6], cur_id;
  static int id;

 public:
  Student();
  void getdata();
  void putdata();
};
int Student::id = 0;
Student::Student() {
  this->id++;
  this->cur_id = this->id;
}
void Student::getdata() {
  std::string temp_name;
  int tempage;

  std::cin >> temp_name >> tempage;
  for (int i = 0; i < 6; i++) {
    std::cin >> this->marks[i];
  }

  this->getbase(temp_name, tempage);
}
void Student::putdata() {
  std::cout << this->putbase();
  int temp = 0;
  for (const auto& i : marks) temp += i;
  std::cout << temp << ' ' << this->cur_id << '\n';
}

int main() {
  int n, val;
  std::cin >> n;  // The number of objects that is going to be created.
  Person* per[n];

  for (int i = 0; i < n; i++) {
    std::cin >> val;
    if (val == 1) {
      // If val is 1 current object is of type Professor
      per[i] = new Professor;

    } else
      per[i] = new Student;  // Else the current object is of type Student

    per[i]->getdata();  // Get the data from the user.
  }

  for (int i = 0; i < n; i++)
    per[i]->putdata();  // Print the required output for each object.

  return 0;
}