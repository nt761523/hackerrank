#include <iostream>

// https://www.hackerrank.com/challenges/c-tutorial-struct/problem?isFullScreen=true

struct Student {
  int age;
  std::string first_name;
  std::string last_name;
  int standard;
};

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  Student s1;
  std::cin >> s1.age >> s1.first_name >> s1.last_name >> s1.standard;
  std::cout << s1.age << ' ' << s1.first_name << ' ' << s1.last_name << ' '
            << s1.standard;
  return 0;
}