#include <algorithm>
#include <iostream>
#include <vector>

// https://www.hackerrank.com/challenges/cpp-lower-bound/problem?isFullScreen=true

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  int count, temp;
  std::cin >> count;
  std::vector<int> basev;
  while (count--) {
    std::cin >> temp;
    basev.push_back(temp);
  }
  std::sort(basev.begin(), basev.end());

  std::cin >> count;
  std::vector<int> searchv;

  for (int i = 0; i < count; i++) {
    std::cin >> temp;
    searchv.push_back(temp);
  }
  std::vector<int>::iterator low;

  for (int i = 0; i < count; i++) {
    low = std::lower_bound(basev.begin(), basev.end(), searchv[i]);
    int index = low - basev.begin();
    basev[index] == searchv[i] ? std::cout << "Yes " : std::cout << "No ";
    std::cout << index + 1 << '\n';
  }
  return 0;
}