#include <iostream>
#include <vector>

// https://www.hackerrank.com/challenges/vector-erase/problem?isFullScreen=true

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  int count, pos, temp, start, end;
  std::vector<int> v;

  std::cin >> count;
  while (count--) {
    std::cin >> temp;
    v.push_back(temp);
  }
  std::cin >> pos >> start >> end;

  v.erase(v.begin() + (pos - 1));
  v.erase(v.begin() + start - 1, v.begin() + end - 1);
  std::cout << v.size() << '\n';
  for (const auto& i : v) std::cout << i << ' ';
  return 0;
}
