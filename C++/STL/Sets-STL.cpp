#include <iostream>
#include <set>
#include <vector>

// https://www.hackerrank.com/challenges/cpp-sets/problem?isFullScreen=true

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  int count, printcount, type, content;
  std::set<int> s;

  std::vector<std::string> out;

  std::cin >> count;
  while (count--) {
    std::cin >> type >> content;
    switch (type) {
      case 1:
        s.insert(content);
        break;
      case 2:
        s.erase(content);
        break;
      case 3:
        std::set<int>::iterator itr = s.find(content);
        std::string temp = (itr == s.end() ? "No" : "Yes");
        out.push_back(temp);

        break;
    }
  }

  for (const std::string& i : out) std::cout << i << '\n';
  return 0;
}
