#include <algorithm>
#include <iostream>
#include <vector>

// https://www.hackerrank.com/challenges/vector-sort/problem?isFullScreen=true

int main() {
  /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  std::vector<int> v;
  int number;
  std::cin >> number;
  int tempi[number];
  while (number--) {
    int temp;
    std::cin >> temp;
    v.push_back(temp);
  }
  std::sort(v.begin(), v.end());

  for (const auto& i : v) std::cout << i << ' ';
  return 0;
}
