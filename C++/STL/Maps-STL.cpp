#include <iostream>
#include <map>

// https://www.hackerrank.com/challenges/cpp-maps/problem?isFullScreen=true

using _mapsi = std::map<std::string,int>;
using _mapii = std::map<int,int>;

void type1(_mapsi &m,std::string &temps,int &tempi){
    _mapsi::iterator itr = m.find(temps);
    if(itr == m.end())
        m.insert(std::make_pair(temps, tempi));
    else
        m[temps] += tempi;
}
void type3(_mapsi &m,std::string &temps,_mapii & out, int &type3count){
    _mapsi::iterator itr = m.find(temps);
    if(itr == m.end())
        out.insert(std::make_pair(type3count, 0));
    else
        out.insert(std::make_pair(type3count, m[temps]));
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int inputcount,type,tempi,type3count;
    std::string temps;
    type3count = 0;
    std::cin >> inputcount;

    _mapsi m;
    _mapii out;
    
    while(inputcount--){
        std::cin >> type >> temps;
        switch (type) {
            case 1:
                std::cin >> tempi;
                type1(m,temps,tempi);
                break;
            case 2:
                m.erase(temps);
                break;
            case 3:
                type3(m, temps, out, type3count);
                type3count++;
                break;
        }
    }
    
    for(int i = 0; i < type3count; i++)
        std::cout << out[i] << std::endl;
    return 0;
}